import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ViewPin {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String pin = reader.readLine();
        reader.close();
        Date currentTime = new Date();
        long startTime = currentTime.getTime();
        int[] code = new int[pin.length()];
        for (int i = 0; i < code.length; i++) {
            code[i] = Integer.parseInt(String.valueOf(pin.charAt(i)));
        }
        List<Neightboors> neightboors = new ArrayList<>();
        for (int i = 0; i < code.length; i++) {
            neightboors.add(new Neightboors(code[i]));
        }
        Combinations combinations = new Combinations(neightboors);
        combinations.combinate(0, 0);

        combinations.getCombinations().forEach(x -> {
            System.out.print(x + " ");
        });
        System.out.println();
        currentTime = new Date();
        long endtime = currentTime.getTime();
        System.out.println("time: " + (endtime - startTime) + " ms");
    }
}
