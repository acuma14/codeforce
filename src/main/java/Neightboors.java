import javafx.util.Pair;

public class Neightboors {

    private static int[][] pinpad = new int[][]{
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9},
            {-1, 0, -1}
    };

    private Pair<Integer, Integer> coords;

    private int[] neightboors = new int[5];

    public Neightboors(int number) {
        this.getCoords(number);
        try {
            neightboors[0] = pinpad[coords.getKey() - 1][coords.getValue()];

        } catch (IndexOutOfBoundsException e) {
            neightboors[0] = -1;
        }
        try {
            neightboors[1] = pinpad[coords.getKey()][coords.getValue() - 1];

        } catch (IndexOutOfBoundsException e) {
            neightboors[1] = -1;
        }
        try {
            neightboors[2] = pinpad[coords.getKey() + 1][coords.getValue()];

        } catch (IndexOutOfBoundsException e) {
            neightboors[2] = -1;
        }
        try {
            neightboors[3] = pinpad[coords.getKey()][coords.getValue() + 1];

        } catch (IndexOutOfBoundsException e) {
            neightboors[3] = -1;
        }
        neightboors[4] =  pinpad[coords.getKey()][coords.getValue()];
    }

    public int[] getNeightboors() {
        return neightboors;
    }

    private void getCoords(int number) {
        for (int i = 0; i < pinpad.length; i++) {
            for (int j = 0; j < pinpad[i].length; j++) {
                if (pinpad[i][j] == number) {
                    this.coords = new Pair<>(i, j);
                    return;
                }
            }
        }
    }
}
