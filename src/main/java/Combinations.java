import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Combinations {

    private static List<Neightboors> neightboors;

    private Set<Integer> combinations = new TreeSet<>();

    public Combinations(List<Neightboors> neightboors) {
        this.neightboors = neightboors;
    }

    public static List<Neightboors> getNeightboors() {
        return neightboors;
    }

    public Set<Integer> getCombinations() {
        return combinations;
    }

    public void combinate(int pointer, int prefix) {
        if (pointer >= getNeightboors().size()) {
            combinations.add(prefix);
            return;
        }
        for (int s : getNeightboors().get(pointer).getNeightboors()) {
            if (s == -1) {
                continue;
            }
            combinate(pointer + 1, concat(prefix, s));
        }
    }

    private int concat(int a, int b) {
        String number = String.valueOf(a) + String.valueOf(b);
        return Integer.valueOf(number);
    }

}
